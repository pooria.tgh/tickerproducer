import model.kucoin.AllSymbolTickerSubscribeMessage
import model.kucoin.AllSymbolTickerSubscribeMessage.toJson
import model.kucoin.Ticker.fromJson
import org.scalatest.wordspec.AnyWordSpecLike

class AllSymbolTickerSubscribeMessageSpec extends AnyWordSpecLike {
  "Test Subscribe message conversion to valid json" in {

    val subscribeMessage =
      AllSymbolTickerSubscribeMessage(
        "1545910660739",
        "subscribe",
        "/market/ticker:all",
        response = true
      )
    val expectedJson =
      "{\"id\":\"1545910660739\",\"type\":\"subscribe\",\"topic\":\"/market/ticker:all\",\"response\":true}"
    val json = toJson(subscribeMessage)

    assert(json === expectedJson)

  }

}
