import model.kucoin.{AllSymbolsTickerSocketAddress, ConnectId, Root, Token}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike

class AllSymbolsTickerWebSocketSpec extends AnyWordSpecLike with Matchers {
  "Test correct SocketAddress" in {
    val root = Root(
      "wss://ws-api.kucoin.com/endpoint"
    )
    val token = Token("myToken")
    val connectId = ConnectId("myId")
    val correctUrl = AllSymbolsTickerSocketAddress(root, token, connectId)

    assert(
      correctUrl.toString === "wss://ws-api.kucoin.com/endpoint?token=myToken&connectId=myId"
    )
  }
}
