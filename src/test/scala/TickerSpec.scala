import model.kucoin.Ticker.{fromJson, toJson}
import model.kucoin.{Data, Ticker}
import org.scalatest.wordspec.AnyWordSpecLike

class TickerSpec extends AnyWordSpecLike {
  "Test Ticker from valid Json conversion" in {

    val expectedTicker =
      Ticker("BTC-USD", "test", "subj", Data(1, 2, 3, 4, 5, 6, 7, 8))

    val json = toJson(expectedTicker)

    assert(fromJson(json) === Right(expectedTicker))

  }

}
