import Repository.PriceChangeAlertRepository
import akka.http.scaladsl.model.DateTime
import model.InMemoryDatabase.TPrice
import model.kucoin.{AllSymbolsTickerSocketAddress, ConnectId, Root, Token}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike

class PriceChangeAlertRepositorySpec extends AnyWordSpecLike with Matchers {
  "Test PriceChangeRepo create " in {
    val repository = new PriceChangeAlertRepository()

    val key = "1"
    val expectedRecord = List(
      TPrice(5, DateTime.now),
      TPrice(15, DateTime.now),
      TPrice(25, DateTime.now)
    )

    repository.create(
      key,
      expectedRecord
    )

    val fetchedRecord = repository.get(key)

    assert(fetchedRecord === Some(expectedRecord))

  }

  "Test PriceChangeRepo update" in {
    val repository = new PriceChangeAlertRepository()

    val key = "1"
    val firstRecords = List(
      TPrice(5, DateTime.now),
      TPrice(15, DateTime.now),
      TPrice(25, DateTime.now)
    )

    val updatedList = List(
      TPrice(5, DateTime.now),
      TPrice(15, DateTime.now),
      TPrice(25, DateTime.now)
    )

    repository.create(
      key,
      firstRecords
    )
    repository.update(key, updatedList)

    val fetchedRecord = repository.get(key)
    assert(fetchedRecord === Some(updatedList))

  }

  "Test PriceChangeRepo hasChange" in {
    val repository = new PriceChangeAlertRepository()

    val key = "1"

    val expectedRecord = List(
      TPrice(50, DateTime.now),
      TPrice(15, DateTime.now),
      TPrice(25, DateTime.now)
    )

    repository.create(
      key,
      expectedRecord
    )

    val newPrice = TPrice(50, DateTime.now)

    val hasBigChange =
      repository.hasChange(key, newPrice, 0.05)

    assert(hasBigChange)
  }

  "Test PriceChangeRepo Change" in {
    val repository = new PriceChangeAlertRepository()

    val key = "1"

    val expectedRecord = List(
      TPrice(50, DateTime.now),
      TPrice(10, DateTime.now),
      TPrice(25, DateTime.now)
    )

    repository.create(
      key,
      expectedRecord
    )

    val newPrice = TPrice(50, DateTime.now)

    val change =
      repository.PriceChange(key, newPrice)

    assert(change === 4)
  }

}
