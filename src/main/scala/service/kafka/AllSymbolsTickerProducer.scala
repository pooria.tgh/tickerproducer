package service.kafka

import java.util.concurrent.Future

object AllSymbolsTickerProducer {
  import org.apache.kafka.clients.producer._

  import java.util.Properties

  def connect(): KafkaProducer[String, String] = {
    val props = new Properties()
    props.put("bootstrap.servers", "localhost:9092")
    props.put(
      "key.serializer",
      "org.apache.kafka.common.serialization.StringSerializer"
    )
    props.put(
      "value.serializer",
      "org.apache.kafka.common.serialization.StringSerializer"
    )
    val producer = new KafkaProducer[String, String](props)
    producer
  }

  def writeToTopic(
      producer: KafkaProducer[String, String],
      producerRecord: ProducerRecord[String, String]
  ): Future[RecordMetadata] = {
    producer.send(producerRecord)
  }

  def disconnect(producer: KafkaProducer[String, String]): Unit = {
    producer.close()
  }
}
