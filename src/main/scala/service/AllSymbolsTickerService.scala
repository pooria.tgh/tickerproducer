package service

import akka.http.scaladsl.model.ws.Message
import model.kucoin.{AllSymbolsTickerSocketAddress, ConnectId, Root, Token}
import org.ini4j.{Profile, Wini}
import service.kucoin.AllSymbolsTickerWebSocket

import java.io.{File, FileNotFoundException, IOException}
import scala.concurrent.Promise

/**
  * This service transform the socket data to BTicker
  * {{{
  *      +------------------------------------------+
  *      | Resulting                                |
  *      |                                          |
  *      |       +-------+               +------+   |
  *      |       |       |               |       |  |
  * socket ~~>   |  JSON | ~CONVERT  ~>  |tickers|  |
  *      |       |       |               |       |  |
  *      |       +-------+               +------+   |
  *      +------------------------------------------+
  * }}}
  * The results will be send to a kafka topic as {Prices:{ask:<>,bid:<>},size}}
  */

object AllSymbolsTickerService extends ServiceInterpreter {

  var socketConfig: AllSymbolsTickerSocketAddress = _
  val socket = AllSymbolsTickerWebSocket
  var promise: Promise[Option[Message]] = _

  def start(configPath: String): Unit = {
    socketConfig = readConfig(configPath)
    promise = socket.connect(socketConfig)
  }

  def stop(): Unit = {
    socket.disconnect(promise)
  }

  def readConfig(configPath: String): AllSymbolsTickerSocketAddress = {

    val ini: Option[Wini] = {
      try {
        Some(new Wini(new File(configPath)))
      } catch {
        case x: FileNotFoundException =>
          println(x) //log
          None
        case x: IOException =>
          println(x) //log
          None
      }
    }

    val socketAddress = ini match {

      case Some(value) =>
        val kucoinSocketSection: Profile.Section = value.get("KucoinWebSocket")
        val root = Root(kucoinSocketSection.get("socket.root", classOf[String]))
        val token = Token(
          kucoinSocketSection.get("socket.token", classOf[String])
        )
        val connectId = ConnectId(
          kucoinSocketSection.get("socket.connectId", classOf[String])
        )
        AllSymbolsTickerSocketAddress(root, token, connectId)

      case None =>
        throw new FileNotFoundException("Config file not found")
    }
    socketAddress
  }

}
