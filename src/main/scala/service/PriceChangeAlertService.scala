package service

import Repository.PriceChangeAlertRepository
import model.InMemoryDatabase.TPrice
import model.buxc.BTicker
import org.apache.kafka.streams.{KafkaStreams, StreamsConfig}
import org.apache.kafka.streams.scala.StreamsBuilder
import org.apache.kafka.streams.scala.kstream.KStream
import akka.http.scaladsl.model.DateTime
import model.buxc.BTicker.toJson

import java.time.Duration
import java.util.Properties
import scala.math.Numeric.BigDecimalAsIfIntegral.abs

/**
  * This service calculates the price change for all Tickers from Service AllSymbolsTickerService
  * {{{
  *     +------------------------------------+
  *     | Resulting                          |
  *     |                                    |
  *     |  +-------+               +------+  |
  *     |  |       |               |      |  |
  * In ~~> |BTicker| ~is alert?~>  | alert|  |
  *     |  |       |               |      |  |
  *     |  +-------+               +------+  |
  *     +------------------------------------+
  * }}}
  * The results will be send to a kafka topic as alert format (key,change)
  */

object PriceChangeAlertService extends ServiceInterpreter {
  import org.apache.kafka.streams.scala.ImplicitConversions._
  import org.apache.kafka.streams.scala.Serdes._

  val db = new PriceChangeAlertRepository()

  override def start(configPath: String): Unit = {

    //step 0: read config for kafka
    val config = getConfig(configPath)

    //step 1: create stream from input data
    val builder = new StreamsBuilder
    val tickersStream: KStream[String, String] =
      builder.stream[String, String]("tickers")

    //step 2: map json to object for input stream
    val tickersObjectStream: KStream[String, BTicker] =
      tickersStream.mapValues(p =>
        BTicker.fromJson(p) match {
          case Right(value) => value
        }
      )

    //step 3: check alert conditions in threshold time
    val alertStream = tickersObjectStream.map { (key, value) =>
      db.get(key) match {

        case Some(records) =>
          val filteredByTimeThreshold =
            records.filter(p =>
              p.create.plus(86000).clicks > DateTime.now.clicks
            )

          val updatedTickerList =
            TPrice(value.prices.ask, DateTime.now) :: filteredByTimeThreshold

          db.update(
            key,
            updatedTickerList
          )

          val isAlert =
            db.PriceChange(
              key,
              TPrice(value.prices.ask, DateTime.now)
            )
          (key, isAlert)

        case _ =>
          db.create(key, List(TPrice(value.prices.ask, DateTime.now)))
          (key, BigDecimal.decimal(0))
      }
    }

    //step 4: send alert with true status to alert topic
    alertStream
      .filter((k, v) => v >= 0.05)
      .map((k, v) => (k, (k, v).toString))
      .to("alert")

    //step 5: send non-alert with false status to non-alert topic
    alertStream
      .filter((k, v) => v < 0.05)
      .map((k, v) => (k, (k, v).toString))
      .to("nalert")

    val streams: KafkaStreams = new KafkaStreams(builder.build(), config)
    streams.start()

    sys.ShutdownHookThread {
      streams.close(Duration.ofSeconds(10))
    }

  }

  override def stop(): Unit = ???

  def getConfig(configPath: String): Properties = {
    val config: Properties = {
      val p = new Properties()
      p.put(StreamsConfig.APPLICATION_ID_CONFIG, "PriceChangeAlertService")
      val bootstrapServers = "localhost:9092"
      p.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers)
      p
    }
    config
  }

}
