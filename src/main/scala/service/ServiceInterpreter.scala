package service

trait ServiceInterpreter {
  def start(configPath: String)
  def stop()
}
