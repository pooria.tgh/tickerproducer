package service.kucoin

import akka.Done
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.ws.{Message, TextMessage, WebSocketRequest}
import akka.stream.scaladsl.{Flow, Keep, Sink, Source}
import model.buxc.BTicker
import model.kucoin.AllSymbolTickerSubscribeMessage.toJson
import model.kucoin.{
  AllSymbolTickerSubscribeMessage,
  AllSymbolsTickerSocketAddress
}
import model.kucoin.Ticker.{fromJson, toBTicker}
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import service.kafka.AllSymbolsTickerProducer

import scala.concurrent.{Future, Promise}
import scala.language.implicitConversions

object AllSymbolsTickerWebSocket {

  implicit val system: ActorSystem = ActorSystem()

  val kafkaProducer: KafkaProducer[String, String] =
    AllSymbolsTickerProducer.connect()

  implicit def transformer(
      tickerJson: String
  ): Either[String, (String, BTicker)] = {
    fromJson(tickerJson) match {
      case Right(value) => Right((value.subject, toBTicker(value)))
      case _            => Left("Invalid Json")
    }
  }

  implicit val sourceMessages: List[TextMessage.Strict] =
    List(
      TextMessage(
        toJson(
          AllSymbolTickerSubscribeMessage(
            "1545910660739",
            "subscribe",
            "/market/ticker:all",
            response = true
          )
        )
      )
    )

  def sink(implicit
      transform: String => Either[String, (String, BTicker)]
  ): Sink[Message, Future[Done]] =
    Sink.foreach[Message] {
      case message: TextMessage.Strict =>
        val transformedTicker = transform(message.text).map(p =>
          new ProducerRecord("tickers", p._1, BTicker.toJson(p._2))
        )

        transformedTicker match {
          case Right(value) =>
            AllSymbolsTickerProducer.writeToTopic(
              kafkaProducer,
              value
            ) //kafkaProducer.send(value)
          case _ => //log
        }

        println(transformedTicker)
      case _ =>
      // Proper log
    }

  def source(implicit
      textMessages: List[TextMessage]
  ): Source[Message, Promise[Option[Message]]] = {
    Source(textMessages).concatMat(Source.maybe[Message])(Keep.right)
  }

  def flow(): Flow[Message, Message, Promise[Option[Message]]] = {
    Flow.fromSinkAndSourceMat(
      sink,
      source
    )(Keep.right)
  }

  def connect(
      allSymbolsTickerSA: AllSymbolsTickerSocketAddress
  ): Promise[Option[Message]] = {
    val (upgradeResponse, promise) = {
      Http().singleWebSocketRequest(
        WebSocketRequest(
          allSymbolsTickerSA.toString
        ),
        flow()
      )
    }
    promise
  }

  def disconnect(promise: Promise[Option[Message]]): Unit = {
    kafkaProducer.close()
    promise.success(None)
    system.terminate()
  }

}
