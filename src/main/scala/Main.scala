import service.{AllSymbolsTickerService, PriceChangeAlertService}

object Main extends App {

  /**
    * Run Task1 and Task2 as two different services
    *
    * arg(0) full path to config.ini file
    */

  if (args.length == 0) {
    println("define config file path")
  } else {
    val filePath = args(0)
    AllSymbolsTickerService.start(filePath)

    PriceChangeAlertService.start(filePath)
  }
}
