package model.buxc

import io.circe
import io.circe.parser.decode
import io.circe.syntax.EncoderOps

import scala.language.implicitConversions

case class BTicker(prices: Price, size: BigDecimal)
case class Price(bid: BigDecimal, ask: BigDecimal)

object BTicker {
  import io.circe.generic.auto._

  implicit def toJson(bTicker: BTicker): String = {
    bTicker.asJson.noSpaces
  }

  def fromJson(json: String): Either[circe.Error, BTicker] = {
    decode[BTicker](json)
  }

}
