package model.InMemoryDatabase

import akka.http.scaladsl.model.DateTime

case class TPrice(price: BigDecimal, create: DateTime)
