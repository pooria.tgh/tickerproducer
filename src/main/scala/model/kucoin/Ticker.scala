package model.kucoin

import io.circe.syntax.EncoderOps
import model.buxc.{BTicker, Price}

case class Ticker(`type`: String, topic: String, subject: String, data: Data)
case class Data(
    bestAsk: BigDecimal,
    bestAskSize: BigDecimal,
    bestBid: BigDecimal,
    bestBidSize: BigDecimal,
    sequence: BigDecimal,
    price: BigDecimal,
    size: BigDecimal,
    time: BigDecimal
)

object Ticker {
  import io.circe._, io.circe.generic.auto._, io.circe.parser._

  def fromJson(json: String): Either[Error, Ticker] = {
    val decodedTicker = decode[Ticker](
      json
    )
    decodedTicker
  }

  def toJson(ticker: Ticker): String = {
    ticker.asJson.noSpaces
  }

  def toBTicker(ticker: Ticker): BTicker = {
    val price = Price(ticker.data.bestBid, ticker.data.bestAsk)
    BTicker(price, ticker.data.size)
  }

}
