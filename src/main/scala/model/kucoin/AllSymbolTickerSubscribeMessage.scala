package model.kucoin

import io.circe.syntax.EncoderOps

case class AllSymbolTickerSubscribeMessage(
    id: String,
    `type`: String,
    topic: String,
    response: Boolean
)
object AllSymbolTickerSubscribeMessage {
  import io.circe.generic.auto._

  def toJson(subscribeMessage: AllSymbolTickerSubscribeMessage): String =
    subscribeMessage.asJson.noSpaces
}
