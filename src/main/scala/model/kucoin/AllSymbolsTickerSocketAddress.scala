package model.kucoin

case class Root(url: String)

case class Token(value: String) {
  override def toString: String = "token=" + value
}

case class ConnectId(value: String) {
  override def toString: String = "connectId=" + value
}

case class AllSymbolsTickerSocketAddress(
    root: Root,
    token: Token,
    connectId: ConnectId
) {
  override def toString: String = root.url + "?" + token + "&" + connectId
}
