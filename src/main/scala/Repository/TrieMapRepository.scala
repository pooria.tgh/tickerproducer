package Repository

import scala.collection.concurrent.TrieMap

class TrieMapRepository[A, B] {
  private val cache = new TrieMap[A, B]

  def create(key: A, value: B): Option[B] = {
    cache.put(key, value)
  }

  def update(key: A, value: B): Unit = {
    cache.update(key, value)
  }

  def get(key: A): Option[B] =
    cache.get(key)

  def delete(key: A): Option[B] =
    cache.remove(key)
}
