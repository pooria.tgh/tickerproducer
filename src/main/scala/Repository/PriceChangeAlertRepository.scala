package Repository

import akka.http.scaladsl.model.DateTime
import model.InMemoryDatabase.TPrice

import scala.math.Numeric.BigDecimalAsIfIntegral.abs

class PriceChangeAlertRepository
    extends TrieMapRepository[String, List[TPrice]] {

  def calculateChange(
      oldValue: BigDecimal,
      newValue: BigDecimal
  ): BigDecimal = {
    abs(oldValue - newValue) / oldValue
  }
  def hasChange(
      key: String,
      newRecord: TPrice,
      threshold: BigDecimal
  ): Boolean = {
    this.get(key) match {
      case Some(oldRecord) => {
        val biggestDifList = oldRecord
          .filter(p => p.price != 0)
          .map(p => calculateChange(p.price, newRecord.price))
          .filter(p => p > threshold)
        if (biggestDifList.isEmpty)
          false
        else
          true
      }
      case _ => false
    }
  }

  def PriceChange(
      key: String,
      newRecord: TPrice
  ): BigDecimal = {
    this.get(key) match {
      case Some(oldRecord) => {
        val biggestDifList = oldRecord
          .filter(p => p.price != 0)
          .map(p => calculateChange(p.price, newRecord.price))
        if (biggestDifList.isEmpty)
          -100
        else
          biggestDifList.max
      }
      case _ => -200
    }
  }

}

object PriceChangeAlertRepository {
  def apply(): PriceChangeAlertRepository = {
    new PriceChangeAlertRepository()
  }
}
