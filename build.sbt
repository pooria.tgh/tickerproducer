name := "TickerProducer"

version := "0.1"

scalaVersion := "2.13.6"

val AkkaVersion = "2.6.16"
val AkkaHttpVersion = "10.2.6"
val circeVersion = "0.14.1"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-stream-testkit" % AkkaVersion % Test,
  "com.typesafe.akka" %% "akka-testkit" % AkkaVersion % Test,
  "com.typesafe.akka" %% "akka-stream" % AkkaVersion,
  "com.typesafe.akka" %% "akka-http" % AkkaHttpVersion,
  "io.circe" %% "circe-core" % circeVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "io.circe" %% "circe-parser" % circeVersion,
  "org.ini4j" % "ini4j" % "0.5.4",
  "org.apache.kafka" % "kafka-clients" % "3.0.0",
  "ch.qos.logback" % "logback-classic" % "1.1.3" % Runtime,
  "org.scalatest" %% "scalatest" % "3.2.9" % "test",
  "org.apache.kafka" % "kafka-clients" % "3.0.0",
  "org.apache.kafka" % "kafka-streams" % "2.0.0",
  "org.apache.kafka" %% "kafka-streams-scala" % "3.0.0"
)
