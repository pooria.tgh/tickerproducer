# Tickers from Kucoin Project

### Getting Started

How much coverage did I get for these critical non-functional requirements:

| Name | 0 to 5         |
| ------------- | ----------- |
| Security| 0 |
| Availability| 1  |
| Usability| 2| 
| Scalability | 2 | 
| Recoverability   | 2 | 
| Performance| 2 | 

What aspects of functional requirements have I met?

| Name | check         |
| ------------- | ----------- |
| Task 1| :white_check_mark: |
| Task 2| :white_check_mark: |
| docker-compose| :warning:| 

:warning: Only for zoopkeer and kafka 

## Code Instruction
In these three illustrations you can see what each service is all about.
### General design
I connect task 1 and task 2 services with a Kafka topic. The first service sends incoming data from a WebSocket to a topic. The second service consumes earlier step messages, calculates the price difference, and sends the alert message to a different topic.
![Alt-Text](https://miro.medium.com/max/1400/1*SepZaVlL5EZK2Tv9AVdi_w.jpeg)

### Task 1
As part of task one, I connected with the Akka-HTTP library to their socket, subscribing to AllSymbolTickers, and sending the proper result format to a Kafka topic.

- Important decisions:
    - It is assumed that the token will be provided, but I will implement a method to retrieve it from their API if it is not.
    - To prevent hardcoding, such as WebSocket addresses, I created a config file for this service.

![Alt-Text](https://miro.medium.com/max/2000/1*Ugum9gh-5RZUX1KvtzYvvw.jpeg)

### Task 2
I found this task interesting and challenging. I reached some solutions such as:

1- Using Apache Flink, which has Delta-Driven Windows operation, is very similar to this problem.
![Alt-Text](https://flink.apache.org/img/blog/blog_data_driven.png)

2- Using Window aggregation query with Kafka-Stream or Akka-Stream

3- Keeping the 24h records in memory using Redis, which has expiration for its key, value

4 - Using the in-memory data structure, keep the 24h records in memory.

I decided to go with the last choice because I wanted to keep this service less dependent on other external services and more deployable. Also, I considered that keeping 24 hours data may cause a problem, but for this, I will and broke the information horizontally based on their Subject(BTC-DST,...) and keep track of the memory.



![Alt-Text](https://miro.medium.com/max/1400/1*V8U8c9NOni7f8KTYI-gz1w.jpeg)

### Technology
  0. Scala "2.13.6" 
  1. "akka-stream"
  2. "akka-http"
  3. "io.circe" 
  4. "org.ini4j"
  5. "kafka-clients"
  6. "scalatest"

### Test
I wrote some basic test to show I can understand the concept but because of time I could'nt wrote test for all aspect of the services and I just trust my eys in this case.

### Next Step
1. put all topic names on config file
2. put all conditions such as 5% change and 24h as a threshold on config file
3. add log with level configuration
4. better Exception handling

## How to run

I use sbt assembly to publish my works and then obfuscate the jar file with a tool.

1. sbt assembly/sbt build for getting jar file in the target directory
2. Update config.ini file with a proper token and other configurations.
3. run docker-compose to run Zookeeper and Kafka
  - All configs are on the default port and local.
  - I will move these to the config file in actual production.
4. java -jar [jar file] [path to config file]

or watch short demo:
[demo](https://youtu.be/-2OSvi_yQ90)




